package AngajatiApp.repository;
import AngajatiApp.model.Employee;
import org.junit.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import static AngajatiApp.controller.DidacticFunction.*;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotSame;

public class EmployeeMockTest {

    EmployeeRepositoryInterface employees = new EmployeeMock();

    private static Employee e1, e2;

    @BeforeEach
    public void setUp() throws Exception {
        e1 = new Employee("AAAA", "BBAA", "2999999334432", ASISTENT, 222.2);
        e2 = new Employee("A", "BB", "2999999334432", ASISTENT, 222.2);
        employees.addEmployee(e1);
        employees.addEmployee(e2);
    }

    @AfterEach
    public void tearDown() throws Exception {
        e1 = e2 = null;
    }

    @Test
    public void modifyEmployeeFunction1() {
        List<Employee> temp = employees.getEmployeeList();
        for (Employee e : temp) {
            if (e.getId() == 0) employees.modifyEmployeeFunction(e, LECTURER);
            assertEquals(LECTURER,e.getFunction());
            assertEquals(true,e != null);
        }
    }


    @Test
    public void modifyEmployeeFunction2() {
        Employee e = new Employee(null,null,null,null,null);
        employees.modifyEmployeeFunction(e, LECTURER);
        assertNotSame(LECTURER,e.getFunction());
    }

    @Test
    public void modifyEmployeeFunction3() {
        e1 = new Employee("AAAA", "BBAA", "2999999334432", ASISTENT, 222.2);
        EmployeeRepositoryInterface tempEmployees = new EmployeeMock();
        //checks that the employeeList.size == 0
        assertEquals(0,tempEmployees.getEmployeeList().size());

        tempEmployees.modifyEmployeeFunction(e1,LECTURER);
        assertNotSame(LECTURER,e1.getFunction());
    }

    @Test
    public void modifyEmployeeFunction4() {
        Employee eTemp = new Employee("AAAA", "BBAA", "2999999334432", ASISTENT, 222.2);
        eTemp.setId(100);
        employees.modifyEmployeeFunction(eTemp,LECTURER);
        assertNotSame(LECTURER,eTemp.getFunction());
    }

}